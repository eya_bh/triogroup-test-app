import {observable, action, makeAutoObservable} from "mobx"
import { createContext } from "react"
import axios from "axios";

class ProductsStore {
    constructor() {
        makeAutoObservable(this);
        this.getProducts();
    }

    @observable products = {
        loading: false,
        data: null,
        error: false
    };

    @action getProducts = () => {
        if(this.products.data) {
            return this.products;
        } else {
            const apiUrl = 'https://www.polyclip.com/de/api/products';
            axios(apiUrl,{
                method: 'get',
            }).then(response => {
                this.products = {
                    loading: true,
                    data: response.data.products,
                    error: false
                };
            }).catch(() => {
                this.products = {
                    loading: false,
                    data: null,
                    error: true
                };
            })
            return this.products;
        }
    }
}

export default createContext(new ProductsStore())