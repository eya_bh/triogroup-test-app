import React from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
} from "react-router-dom";
import {Navbar, Nav} from 'react-bootstrap'
import Logo from '../Assets/images/logo.svg';
import Products from "../Pages/Products";
import ProductDetails from "../Pages/ProductDetails";

class NavigationBar extends React.Component{

    render() {
        return(
            <Router>
                <Navbar bg="light" variant="light" expand="lg" sticky="top">
                    <div className={"container-fluid"}  >
                        <Link to={'/'} className="custom-width">
                            <Navbar.Brand>
                                <img
                                    alt="logo"
                                    src={Logo}
                                    className="d-inline-block align-top half-width"
                                />
                            </Navbar.Brand>
                        </Link>

                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <Link to={'/'} className={'nav-link'}>Startseite</Link>
                            </Nav>
                        </Navbar.Collapse>
                    </div>
                </Navbar>

                <Routes>
                    <Route path="/" element={<Products />}/>
                    <Route path="/:sku" element={<ProductDetails />}/>
                </Routes>
            </Router>
        )
    }
}
export default NavigationBar;