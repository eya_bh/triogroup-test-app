import React from 'react'
import {Container} from 'react-bootstrap'

class NavigationBar extends React.Component{
    render(){
        return(
            <footer>
                <Container>
                    <div className="row mt-4">
                        <div className="col-md-12">
                            <div className="footer-bottom">
                                <p className="text-xl-center">
                                    &copy;{new Date().getFullYear()} trio-group - All Rights Reserved
                                </p>
                            </div>
                        </div>
                    </div>
                </Container>
            </footer>
        )
    }
}
export default NavigationBar;