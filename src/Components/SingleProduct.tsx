import React from 'react';
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

interface Props {
    product: any;
}

interface State {}

class SingleProduct extends React.Component<Props, State>{
    render(){
        return(
            <div className="card-wrapper">
                <div>
                    <img className="img-fluid my-2 max-h-170"
                         src={this.props.product.image}
                         alt={"img-"+this.props.product.id}/>
                </div>
                <Card.Body className="text-center">
                    <h5 className="card-title text-black">{this.props.product.title}</h5>
                    <Card.Text className="text-wrapper">
                        {this.props.product.subtitle}
                    </Card.Text>
                    <Link to={'/'+this.props.product.id}
                          className="btn btn-secondary d-block card-link">Mehr erfahren</Link>
                </Card.Body>
            </div>
        )
    }
}
export default SingleProduct;