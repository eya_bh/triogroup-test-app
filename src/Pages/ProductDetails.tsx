import React, {useState, useEffect} from 'react';
import {Container, Spinner, Table} from "react-bootstrap";
import { useParams } from 'react-router-dom';
import axios from "axios";

const ProductDetails = () => {
    const params = useParams();
    const [product, setProduct] = useState({
        loading: false,
        data: null,
        error:false
    })

    const apiUrl = 'https://www.polyclip.com/de/api/products/'+params.sku;
    useEffect(() => {
        setProduct({
            loading: true,
            data: null,
            error: false
        })
        axios({
            method: 'get',
            url: apiUrl,
        }).then(response => {
            setProduct({
                loading: true,
                data: response.data.products[0],
                error: false
            })
        })
        .catch(() => {
            setProduct({
                loading: false,
                data: null,
                error: true
            })
        })
    }, [apiUrl])

    let content = null;

    if(product.error){
        content = <p>An error has occured!</p>;
    }
    if(product.loading){
        content = (
            <div className="row">
                <div className="col-md-12 text-center">
                    <Spinner animation="border" role="status" className="text-center">
                        <span className="visually-hidden">loading...</span>
                    </Spinner>
                </div>
            </div>
        );
    }

    function getProductDataByOption(productData: any, option: string) {

        const keyArray: any = {
            processing: 'Verarbeitung',
            packaging: 'Verpackung',
            operating_mode: 'Betriebsart',
            calibre_weight: 'Kaliber/Gewicht',
            shift_operation: 'Schichtbetrieb',
        };

        try{
            if(productData[option].length === 1){
                return <li>{keyArray[option]}: <span dangerouslySetInnerHTML={{__html: productData[option][0].name }} /></li>;
            } else {
                return (
                    <li>
                        {keyArray[option]}:
                        <ul>
                            {productData[option].map((entry: any) => (
                                <li>
                                    <span dangerouslySetInnerHTML={{__html: entry.name }} />
                                </li>
                            ))}
                        </ul>
                    </li>
                )
            }
        } catch (e){
            return null;
        }
    }

    function getProductTechnicalData(productData: any) {
        try{
            if(productData['tech_data'].length > 0){
                return (

                    <Table striped>
                        <thead>
                        <th colSpan={2}><span>TECHNISCHE DATEN</span></th>
                        </thead>
                        <tbody>
                        {productData['tech_data'].map((entry: any) => (
                            <tr>
                                <td>{entry.name}</td>
                                <td>
                                    {(entry.min && entry.max) ?
                                        <span
                                            dangerouslySetInnerHTML={{__html: entry.min +
                                                    ' - ' +
                                                    entry.max +
                                                    ' ' +
                                                    entry.unit }} />
                                        : <span
                                            dangerouslySetInnerHTML={{__html: entry.default +
                                                    ' ' + entry.unit }} />}
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                )
            }
        } catch (e){
            return null;
        }
    }

    if(product.data) {
        const productData : any = product.data;
        content = (
            <React.Fragment>
                <div className="col-lg-3 col-md-4 p-2 text-center">
                    <div className="border img-details">
                        <img
                            alt={'img-details-'+productData.id}
                            src={productData.image}
                            className="img-fluid mb-4 full-width p-3"
                        />
                    </div>
                </div>
                <div className="col p-2">
                    <h1>{productData.title}</h1>
                    <h5>{productData.subtitle}</h5>
                    <ul>
                        {getProductDataByOption(productData,'processing')}
                        {getProductDataByOption(productData,'packaging')}
                        {getProductDataByOption(productData,'operating_mode')}
                        {getProductDataByOption(productData,'calibre_weight')}
                        {getProductDataByOption(productData,'shift_operation')}
                    </ul>
                    {getProductTechnicalData(productData)}
                </div>
            </React.Fragment>
        )
    }
    return (
        <Container>
            <div className="row">
                <h1 className="my-4">Produktdaten</h1>
                {content}
            </div>
        </Container>
    );
};

export default ProductDetails;