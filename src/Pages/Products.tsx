import React, {useState, useEffect, useContext} from 'react';
import {Container, FormControl, Spinner} from "react-bootstrap";
import { observer } from "mobx-react";

import SingleProduct from "../Components/SingleProduct";
import ProductsStore from "../Stores/ProductsStore";

const Products = () => {
    const productsStore = useContext(ProductsStore);
    const { products } = productsStore;

    const [searchTerm, setSearchTerm] = useState("");

    useEffect(() => {
        productsStore.getProducts();
    }, [])

    let content = null;

    if(products.error){
        content = <p>An error has occured!</p>;
    }
    if(products.loading){
        content = (
            <div className="row">
                <div className="col-md-12 text-center">
                    <Spinner animation="border" role="status" className="text-center">
                        <span className="visually-hidden">loading...</span>
                    </Spinner>
                </div>
            </div>
        );
    }
    if(products.data){
        const allProducts : any = products.data;
        let filteredProducts : any;

        filteredProducts = allProducts.filter((product : any) => {
            if(searchTerm === "") {
                return product;
            } else if((product.title.toLowerCase().includes(searchTerm.toLowerCase())) ||
                (product.subtitle.toLowerCase().includes(searchTerm.toLowerCase()))
            ) {
                return product;
            }
        });
        if(filteredProducts.length > 0) {
            content = filteredProducts.map((product: any, key: number) =>
                <div className="col-12 col-md-4 col-lg-4 align-items-stretch mb-4" key={key}>
                    <SingleProduct product={product}/>
                </div>
            )
        } else {
            content = (
                <div className="col-12 col-md-4 col-lg-4 align-items-stretch mb-4">
                    <p>kein Produkt gefunden!</p>
                </div>
            );
        }
    }
    return (
        <Container>
            <div className="row mb-3">
			    <h1 className="my-4">Produkte</h1>
                <div className="col-md-4">
                    <FormControl
                        type="text"
                        id="product-search"
                        placeholder="Suche ..."
                        onChange={(event : any) => {setSearchTerm(event.target.value)}}
                    />
                </div>
            </div>
            <div className="row">
                {content}
            </div>
        </Container>
    );
};

export default observer(Products);