import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './Assets/css/App.css';

import NavigationBar from "./Components/NavigationBar";
import Footer from "./Components/Footer";

function App() {
  return (
      <div>
          <NavigationBar />
          <Footer/>
      </div>
  );
}

export default App;
